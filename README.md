# Universit&eacute; De Borgogne -- Ecole D'Hiver


## Here you will find:

- The pdf for workshop number 1 on Geostatistics
- The pdf for workshop number 2 on Point Processes
- a folder containing a 'level 5' shapefile of France, from GADM, the Database of Global Administrative Areas
- a 100m x 100m modelled population density raster for France from Worldpop


## License
Please see the licences from the GADM and Worldpop sites for respectively the shapefile and the raster
